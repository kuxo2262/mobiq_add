function [ obj_meas ] = combine_binQ_OPM_TiH2021(obj_meas_mon, obj_meas_bin)
% combine_binQ_OPM_TiH2021.m  Function combines the monaural quality measure (OPM) and the binaural 
% quality measure(binQ) to calculate an overall audio quality measure as suggested by Biberger et al. (2021).
% 
% INPUT:        
%  obj_meas_mon:        monaural output measure (OPM) of GPSMq (Biberger et al., 2018)
%  obj_meas_bin:        binaural output measure (binQ) of BAM-Q (Fle�ner et al., 2017)
% 
% OUTPUT:
%  obj_meas:           combined output measure (see Biberger et al., 2021)
% 
% Usage: [obj_meas] = combine_binQ_OPM_TiH2021(obj_meas_mon, obj_meas_bin)
% thomas.biberger@uni-oldenburg.de;
% date: 2021-01-14
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2021, Jan-Hendrik Fle�ner, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

obj_meas_mon(obj_meas_mon>100)=100; % limitation of upper boundary -> this is only relevant if reference vs. reference is tested
obj_meas_bin(obj_meas_bin>100)=100; % limitation of upper boundary -> this is only relevant if reference vs. reference is tested
vParams=[0.1188, 38.9817,0.0192,-20.8263]; % based on monaural and binaural outputs of MoBI-Q
mon_tmp=1./(1 + exp(-vParams(1).*(obj_meas_mon-vParams(2)))); % apply sigmoid to monaural scores
bin_tmp=1./(1 + exp(-vParams(3).*(obj_meas_bin-vParams(4)))); % apply sigmoid to monaural scores
obj_meas=(mon_tmp+bin_tmp); % combine monaural and binaural scores
obj_meas(obj_meas<0.6083)=0.6083; % ensures that the combined score does not fall below 0.6083,
                                  % while such low values (<0.6083) were never
                                  % observed for the distortioned signals in 
                                  % the databases used so far (even not for 
                                  % the anchor signals) 
obj_meas=(obj_meas-0.6083)./(1.9098-0.6083); % bound quality scores between 0 and 1

end
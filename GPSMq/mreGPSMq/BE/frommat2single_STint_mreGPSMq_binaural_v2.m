% function [SNR_mat_inc, SNR_mat_dec, SNR_mat_STint_inc, SNR_mat_STint_dec, mCorr]=frommat2single_STint_mreGPSMq_binaural_v2(Ref,Test,params,mCorr) 
function [BE]=frommat2single_STint_mreGPSMq_binaural_v2(Ref,Test,params)                
% frommat2single_STint_mreGPSMq.m  function that calculates increment and decrement
% SNRs based on power and envelope power features
% 
% input:
%       Ref:          Power and envelope power features of the clean signal
%       Test:         Power and envelope power features of the distorted signal
%       params:       parameters specifying back-end processing
% 
% output:  
%       BE:           Struct that contains increment and decrement SNRs of
%                     power and envelope power SNRs
% 
% Usage: [BE]=frommat2single_STint_mreGPSMq(Ref,Test,params)
% author: thomas.biberger@uni-oldenburg; jan-hendrik.flessner@uni-oldenburg.de
% date:   2018-05-21
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Jan-Hendrik Fle�ner, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------



%     [noWin noGTFB noMFB]=size(signal) ; % number of modfilterbanks
    noWin =size(Test.Y2,1);
    noGTFB = params.auditory_filt_range.end;
    noMFB = params.mod_filt_range.end;
    
    winSamp=zeros(noGTFB,noMFB);
    signal_temp=zeros(noGTFB,noMFB);
    signal_plot=zeros(noWin,noMFB);
    signal_temp_STint=zeros(noGTFB,noMFB);

    % calculate number of windows
    signal_nWin=squeeze(Test.Y2(:,16,:));
    signal_nWin(signal_nWin~=0)=1;
    nWin=sum(signal_nWin,1);
    

    
%     validate=squeeze(ref(1,:,1));
    validate_ref=squeeze(Ref.Y1(1,:,1));
    validate_sig=squeeze(Test.Y2(1,:,1));

%% ISO threshold values
freqs=[63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 10000 12500 16000];
if params.useISO
    vIsoThrDB = iso389_7(freqs);
else
    vIsoThrDB = zeros(length(freqs),1);
end

for ii = 1:25,
    signal_int(:,ii,:)=max(Test.Y_int2(:,ii,:),1e-10*10^(vIsoThrDB(ii)/10));
    ref_int(:,ii,:)=max(Ref.Y_int1(:,ii,:),1e-10*10^(vIsoThrDB(ii)/10));
end

%% positive SNR mat (increment)
% positive SNRs values if signal > ref  
   SNR_temp_mod_per=(Test.Y2./Ref.Y1)-1; 
   SNR_STint=(signal_int./ref_int)-1;


    SNR_temp_mod_per = min(SNR_temp_mod_per,20);
    SNR_STint = min(SNR_STint,20);
    
    SNR_STint=max(real(SNR_STint),0);
    SNR_temp_mod_per=max(real(SNR_temp_mod_per),0);
   
   dc2mod=max(Test.dc2mod2,0);
   SNR_temp_mod_per=SNR_temp_mod_per.*dc2mod;

    
    for ii=params.auditory_filt_range.start:params.auditory_filt_range.end;  % number of auditory filters
        for kk=1:params.mod_filt_range.end,
                       
            if kk ~= 1,
                for jj = 1:nWin(kk)
                    if signal_int(jj,ii,kk)<=1e-10*10^(vIsoThrDB(ii)/10) || ref_int(jj,ii,kk)<=1e-10*10^(vIsoThrDB(ii)/10)
                        SNR_temp_mod_per(jj,ii,kk) = 0;
                    end
                end
            end

              signal_temp(ii,kk)=nanmean(squeeze(SNR_temp_mod_per(1:nWin(kk),ii,kk)),1);
              signal_temp_STint(ii,kk)=nanmean(squeeze(SNR_STint(1:nWin(kk),ii,kk)),1);
        end
    end
    
    signal_temp_STint=max(signal_temp_STint,0)'; % here all negative values are eliminated
    
    signal_temp=signal_temp';
    signal_temp_inc=max(signal_temp,0); % here all negative values are eliminated
  
ModFiltersMatrix = [[1:5 0 0 0 0]; [1:5 0 0 0 0];[1:5 0 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0];...
    [1:7 0 0] ; [1:7 0 0]; [1:7 0 0]; [1:8 0 ]; [1:8 0 ]; [1:8 0 ]; 1:9; 1:9; 1:9; 1:9;...
    1:9; 1:9; 1:9; 1:9; 1:9; 1:9;1:9;1:9;1:9]';


    ModFiltersMatrix=[ones(1,25);ModFiltersMatrix];
    modFiltMat=ones(10,25);
    modFiltMat(ModFiltersMatrix==0)=0;
    modFiltMat = modFiltMat(1:params.mod_filt_range.end,1:params.auditory_filt_range.end);
     

    
    signal_temp_inc=signal_temp_inc.*modFiltMat;

    BE.SNR_mat_inc=signal_temp_inc;
    
    BE.SNR_mat_STint_inc=signal_temp_STint;


%% negative SNR mat (decrement)
% positive SNRs values if signal < ref 
   clear signal_temp
   clear signal_temp_STint
   SNR_temp_mod_per=(Ref.Y1./Test.Y2)-1; 
   SNR_STint=(ref_int./signal_int)-1;


    SNR_temp_mod_per = min(SNR_temp_mod_per,20);
    SNR_STint = min(SNR_STint,20);
    
    SNR_STint=max(real(SNR_STint),0);
    SNR_temp_mod_per=max(real(SNR_temp_mod_per),0);    
     
   dc2mod=max(Ref.dc2mod1,0);
   SNR_temp_mod_per=SNR_temp_mod_per.*dc2mod;

    for ii=params.auditory_filt_range.start:params.auditory_filt_range.end;  % number of auditory filters
        for kk=1:params.mod_filt_range.end,
            if kk ~= 1,
                for jj = 1:nWin(kk)
                    if signal_int(jj,ii,kk)<=1e-10*10^(vIsoThrDB(ii)/10) || ref_int(jj,ii,kk)<=1e-10*10^(vIsoThrDB(ii)/10)
                        SNR_temp_mod_per(jj,ii,kk) = 0;
                    end
                end
            end

              signal_temp(ii,kk)=nanmean(squeeze(SNR_temp_mod_per(1:nWin(kk),ii,kk)),1);
              signal_temp_STint(ii,kk)=nanmean(squeeze(SNR_STint(1:nWin(kk),ii,kk)),1);
        end

    end
    
    signal_temp_STint=max(signal_temp_STint,0)'; % here all negative values are eliminated
       
    signal_temp=signal_temp';
    signal_temp_dec=max(signal_temp,0); % here all negative values are eliminated

    ModFiltersMatrix = [[1:5 0 0 0 0];[1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0];...
    [1:7 0 0] ; [1:7 0 0]; [1:7 0 0]; [1:8 0 ]; [1:8 0 ]; [1:8 0 ]; 1:9; 1:9; 1:9; 1:9;...
    1:9; 1:9; 1:9; 1:9; 1:9; 1:9;1:9;1:9;1:9]';


      ModFiltersMatrix=[ones(1,25);ModFiltersMatrix]; % because of the intensity features
      modFiltMat=ones(10,25);
      modFiltMat(ModFiltersMatrix==0)=0;
      modFiltMat = modFiltMat(1:params.mod_filt_range.end,1:params.auditory_filt_range.end);
    
      signal_temp_dec=signal_temp_dec.*modFiltMat;

    BE.SNR_mat_dec=signal_temp_dec;
     
    BE.SNR_mat_STint_dec=signal_temp_STint;


end
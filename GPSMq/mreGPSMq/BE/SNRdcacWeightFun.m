function [f,SNR_dc_w,SNR_ac_w] = SNRdcacWeightFun(weights, dc, ac)

SNR_dc_w = dc .* weights(1);
SNR_ac_w = ac .* weights(2);

f = 10*log10(SNR_dc_w + SNR_ac_w + eps);
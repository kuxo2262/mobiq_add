function [SNR_dc,SNR_ac]= BackEnd_feature_trafo_mreGPSMq_binaural(BE,params)
% function that transforms envelope power SNRs (modulation features) and
% intensity SNRs (intensity features) into a perceptual measure

% input:
%       signal_inc:         snr-matrix of signal increment (containing short-time modulation and long-time intensity features)
%       signal_dec:         snr-matrix of signal decrement (containing short-time modulation and long-time intensity features)
%       signal_STint_inc:   snr-vector containing increments of short-time intensity features   
%       signal_STint_inc:   snr-vector containing decrements of short-time intensity features 
%       params:             parameters specifying back-end processing
% 
% output:   


signal_inc=BE.SNR_mat_inc;
signal_dec=BE.SNR_mat_dec;
signal_STint_inc=BE.SNR_mat_STint_inc;
signal_STint_dec=BE.SNR_mat_STint_dec;

%% combine increment & decrement SNRs
signal_dec(params.mod_filt_range.start:params.mod_filt_range.end,:) = 10.^(-7/10).*signal_dec(params.mod_filt_range.start:params.mod_filt_range.end,:);

signal_temp = (signal_inc+signal_dec).*0.5;
signal_temp(1,:) = (signal_STint_inc(4,:)+signal_STint_dec(4,:)) .* 0.5;

%% combine across auditory and modulation/intensity channels
signal_temp=signal_temp.^2;

% combining dc-channels
signal_temp_dc=signal_temp(1,params.auditory_filt_range.start:params.auditory_filt_range.end);
SNR_dc=sqrt(sum(signal_temp_dc,2));

% combining ac-channels
signal_temp_ac=signal_temp(params.mod_filt_range.start:params.mod_filt_range.end,params.auditory_filt_range.start:params.auditory_filt_range.end);
SNR_periph_ac=sum(signal_temp_ac,2);
SNR_ac=sqrt(sum(SNR_periph_ac,1));

end
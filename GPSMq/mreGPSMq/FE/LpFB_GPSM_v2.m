function out = LpFB_GPSM_v2(in,freq,fs)
% LpFB_GPSM.m Lowpass-Filterbank applied to power features per auditory
% channel. Temporal-integrated power features are used to normalize short-time envelope power feature.
% (make them independent from the short-time intensity of the carrier)

% INPUT
%   in:         envelope of the corresponding auditory channel
% freq:         vector that gives the modulation frequencies [1,2,4,...,256 Hz]
%   fs:         sampling frequency

% OUTPUT
%   out:        matrix containing dc-values;
%               different window length (depending on the modulation filter center frequency)
%               are used for computing dc-values,
%
% Usage: out = LpFB_GPSM_v2(in,freq,fs)
% author: thomas.biberger@uni-oldenburg
% date:   2013-07-22
% modified: 2018-10-12
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

out=zeros(length(in),length(freq)); % preallocation

factor=1.5;  % 1.4
count =0;

if freq(1)==0;
    start=2;
elseif freq>0;
    start=1;
end

for ii=start:length(freq);
    if freq(ii)==0       
    else
        if freq(ii)<=8;
            
            N=round(fs/(freq(ii)/factor));
            % average window
            b=ones(1,N)/N;  % numerator of the rectangular moving average window
%             out(:,ii) = dlfconvComp(b,in);
            out(:,ii)=moving_average(N,in);
            
        elseif freq(ii)>8 && count==0;
            factor=round(freq(ii)/8);
            count=count+1;
            
            N=round(fs/(freq(ii)/factor));
             % average window
            b=ones(1,N)/N;  % numerator of the rectangular moving average window
%             out(:,ii) = dlfconvComp(b,in);
            out(:,ii)=moving_average(N,in);
                        
        else freq(ii)>8 && count>=1;
            out(:,ii)=out(:,ii-1);
        end
        
    end
end

function stOut = GPSMqBin(RefSig, TestSig, fs)
% GPSMqBin.m  Main function for calculation of audio quality differences between an 
%           unprocessed reference signal and a processed test signal. At first model
%           parameters are defined, followed by the front end and back end
%           calculations resulting in the objective perceptual measure. For
%           more detailed information see Biberger et al. (2018) and
%           Fle�ner et al. (2019)
% 
% input:    RefSig   ... reference signal, 2-dim
%           TestSig  ... test signal, 2-dim, time-aligned with RefSig
%           fs       ... sampling frequency
%
% output:   stOut    ... struct that contains the objective perceptual
%                        measure (OPM) as it is described in Fle�ner et al.(2019)
%                        in Eq. 15 and 16, intensity SNRs (SNR_dc), and envelope
%                        power SNRs (SNR_ac)
%                        
%                         
% Usage: stOut = GPSMqBin(RefSig, TestSig, fs)
% authors: jan-hendrik.flessner@uni-oldenburg.de
%          thomas.biberger@uni-oldenburg.de
% updated 2019-06-02 
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Jan-Hendrik Fle�ner, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

% Back-end related paramter settings
params.auditory_filt_range.start=8;             % audio freqs=[63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 10000 12500 16000]
params.auditory_filt_range.end=24;              % 
params.mod_filt_range.start=3;                  % 3 filter range of modulation filters 2: 1 Hz modulation channel , 10: 256 Hz modulation channel
params.mod_filt_range.end=8;                    % 9 filter range of modulation filters 2: 1 Hz modulation channel , 10: 256 Hz modulation channel
params.useISO=1;                                % 1: use ISO threshold middle ear filter, 0: do not
params.corrThres = 0.8;
%% signal pre-processing (check sig. length, temporal alignment)
tempAlign = 0;
[RefSig TestSig def]= GPSMq_preproc_input(RefSig,TestSig,fs,tempAlign);
work.signal=[RefSig TestSig]; 

%% set model params
[def simdef simwork]=mreGPSMq_param_def(def);

%% Front end calculation
[Ref_l, Ref_r, Test_l, Test_r] = mreGPSMq_preproc_binaural_v2(RefSig,TestSig,def,simdef,simwork,params);

mCorrL = ones(params.mod_filt_range.end,params.auditory_filt_range.end);
mCorrR = ones(params.mod_filt_range.end,params.auditory_filt_range.end);

for kk = params.mod_filt_range.start:params.mod_filt_range.end
    for ll = params.auditory_filt_range.start:params.auditory_filt_range.end
        refL_ = Ref_l.Ymod1(round(def.samplerate_down/4):end,ll,kk);  % default: refL_ = Ref_l.Ymod1(round(fs/4):end,ll,kk);
        refR_ = Ref_r.Ymod1(round(def.samplerate_down/4):end,ll,kk);  % see comment refL_
        signalL_ = Test_l.Ymod2(round(def.samplerate_down/4):end,ll,kk); % see comment refL_
        signalR_ = Test_r.Ymod2(round(def.samplerate_down/4):end,ll,kk); % see comment refL_
        
        mCorrL(kk,ll) = cc(refL_,signalL_);
        mCorrR(kk,ll) = cc(refR_,signalR_);
    end
end
clear Ref_l.Ymod1 Ref_r.Ymod1 Test_l.Ymod2 Test_r.Ymod2 refL_ refR_ signalL_ signalR_

%% Back end calculation
% SNR calculation and temporal averaging
[BE_l]=frommat2single_STint_mreGPSMq_binaural_v2(Ref_l,Test_l,params);
[BE_r]=frommat2single_STint_mreGPSMq_binaural_v2(Ref_r,Test_r,params);

% Combine SNR-values across auditory- and/or modulation channels
[SNR_dc_l,SNR_ac_l]=BackEnd_feature_trafo_mreGPSMq_binaural(BE_l,params);
[SNR_dc_r,SNR_ac_r]=BackEnd_feature_trafo_mreGPSMq_binaural(BE_r,params);

% Combine SNR-values across auditory- and/or modulation channels including
% weighting for making the model less sensitive to IPDs/ITDs
[SNR_dc_fix_l,SNR_ac_fix_l]=BackEnd_feature_trafo_mreGPSMq_binaural_fix(BE_l,params,mCorrL);
[SNR_dc_fix_r,SNR_ac_fix_r]=BackEnd_feature_trafo_mreGPSMq_binaural_fix(BE_r,params,mCorrR);

% average SNRs from the left and right channel
stOut.SNR_dc = 0.5 * (SNR_dc_l + SNR_dc_r);
stOut.SNR_ac = 0.5 * (SNR_ac_l + SNR_ac_r);
stOut.SNR_dc_fix = 0.5 * (SNR_dc_fix_l + SNR_dc_fix_r);
stOut.SNR_ac_fix = 0.5 * (SNR_ac_fix_l + SNR_ac_fix_r);

%% transformation of a single SNR-value into a perceptual measure (OPM*-> see Eq. 15 in Fle�ner et al.,2019)
f = 10*log10(stOut.SNR_dc + stOut.SNR_ac + eps);
b = [-4.08695250298565;75.6467755339438];
stOut.OPM = [f 1]*b;
stOut.OPM(stOut.OPM>100)=100; % limitation of upper boundary -> this is only relevant if reference vs. reference is tested

%% transformation of a single SNR-value into a perceptual measure FIX (OPM*dual-> see Eq. 16 in Fle�ner et al.,2019)
f = 10*log10(stOut.SNR_dc_fix + stOut.SNR_ac_fix + eps);
b = [-4.15742483856597;74.7791007678067];
stOut.OPM_fix = [f 1]*b;
stOut.OPM_fix(stOut.OPM_fix>100)=100; % limitation of upper boundary -> this is only relevant if reference vs. reference is tested

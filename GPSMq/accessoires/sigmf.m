function y = sigmf(x,ampl,params)

y = ampl./(1 + exp(-params(1).*(x-params(2))));